@extends('layouts.index')

@section('content')

<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 stretch-card">
                <div class="card">
                    <div class="card-body">
                        <p class="card-title">Laporan Pembayaran</p>
                        <form method="GET" action="/laporan">
                            @csrf
                            <div class="form-group row">
                                <div class="col-md-2">
                                    @php
                                    $list_bulan = [
                                        '01' => 'Januari',
                                        '02' => 'Februari',
                                        '03' => 'Maret',
                                        '04' => 'April',
                                        '05' => 'Mei',
                                        '06' => 'Juni',
                                        '07' => 'Juli',
                                        '08' => 'Agustus',
                                        '09' => 'September',
                                        '10' => 'Oktober',
                                        '11' => 'November',
                                        '12' => 'Desember',
                                    ];
                                    @endphp
                                    <select name="bulan" id="bulan" required>
                                        @if ($bulan)
                                        <option value="{{ $bulan }}" hidden selected>{{ array_key_exists($bulan, $list_bulan) ? $list_bulan[$bulan]  : 'None' }}</option>
                                        @else
                                        <option value="" hidden selected>Pilih Bulan</option>
                                        @endif
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select name="tahun" id="tahun" required>
                                        @if ($tahun)
                                        <option value="{{ $tahun }}" hidden selected>{{ $tahun }}</option>
                                        @else
                                        <option value="" hidden selected>Pilih Tahun</option>
                                        @endif
                                        <option value="2022">2022</option>
                                        <option value="2021">2021</option>
                                        <option value="2020">2020</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        Filter
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>

                            @endif
                            <table id="tabel_laporan" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal</th>
                                        <th>Nomor Rekening</th>
                                        <th>Nama</th>
                                        <th>Tagihan</th>
                                        <th>Pembayaran</th>
                                        <th>Kembalian</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @if(count($list) > 0)
                                    @foreach($list['show'] as $list)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $list->tanggal_transaksi }}</td>
                                        <td>{{ $list->pelanggan->rekening }}</td>
                                        <td>{{ $list->pelanggan->nama }}</td>
                                        <td>{{ $list->tagihan->tagihan }}</td>
                                        <td>{{ $list->pembayaran }}</td>
                                        <td>{{ $list->kembalian }}</td>
                                        <td style="display: inline-flex">
                                            <button class="btn btn-small btn-warning" style="margin-right: 5px" onclick="window.location.
                                                href='{{ route('laporan.show', $list->id) }}'">Print
                                            </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan=3>Tidak Ada Data</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
        $(document).ready(function() {
            $('#tabel_laporan').DataTable();
        } );
</script>

@endsection

  {{-- <footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
      <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2017 <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap Dash</a>. All rights reserved.</span>
      <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
    </div>
  </footer> --}}
  <!-- partial -->






