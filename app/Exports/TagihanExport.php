<?php

namespace App\Exports;

use App\Model\Tagihan;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\DB;

class TagihanExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $query = "select p.nama nama_pelanggan, p.meteran meteran_awal, t.meteran_baru meteran_akhir, t.volume, t.created_at, t.tagihan from tagihan t left join pelanggan p on p.id = t.id_pelanggan where t.status_bayar = 0";
        // return Tagihan::with('pelanggan')->where('status_bayar', '0')->get();
        $exec = DB::select(DB::raw($query));
        return collect($exec);
    }

    public function headings(): array
    {
        return ["Nama Pelanggan", "Meteran Awal", "Meteran Akhir", "Volume", "Tanggal", "Tagihan"];
    }
}
